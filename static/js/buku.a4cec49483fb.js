$(document).ready(function () {
    $.ajax({
        url: "/daftar_buku/dtJSON/",
        // dataType: "json",
        success: function(data) {
          var result = "";
          var dataOfBook = data.items;
          for (var i = 0; i < dataOfBook.length; i++) {
            result += '<tbody><tr id ="tabel_buku">';
            result += "<td>" + data.items[i].volumeInfo.title + "</td>";
            result += "<td><img src='" + data.items[i].volumeInfo.imageLinks.thumbnail  + "'></td>";
            result += "<td>" + data.items[i].volumeInfo.authors + "</td>";
            result += "<td>" + data.items[i].volumeInfo.publisher + "</td>";
            result +=
              '<td align ="center"><a href="#" class="btn btn-default buttonLove" title="count to favourite"><i class="glyphicon glyphicon-star" ></></a></td>';
            result += "</tr></tbody>";
          }
          $("#tabel").append(result);
        },
        error: function(d) {
          alert("404 detected! Please wait until the File is Loaded.");
        }
      });
