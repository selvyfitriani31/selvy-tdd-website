from django.apps import AppConfig


class MeProfileConfig(AppConfig):
    name = 'me_profile'
