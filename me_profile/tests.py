from django.test import TestCase
from django.test.client import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *

# Create your tests here.
class me_profiletest (TestCase) :
	def test_profile_name_is_exist(self):
		response = Client().get('/profile/')
		self.assertEqual(response.status_code,200)

	def test_profile_using_index_func(self):
		found = resolve('/profile/')
		self.assertEqual(found.func, index)

	def test_profile_using_index_template(self):
		response = Client().get('/hello/')
		self.assertTemplateUsed(response, 'halo.html')

	def test_about_me_is_exist(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('About Me', html_response)


	def test_my_activity_is_exist(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('My Activity', html_response)

	def test_organization_is_exist(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Organization', html_response)

	def test_achievement_is_exist(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Achievement', html_response)

