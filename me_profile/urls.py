from django.urls import path
from me_profile import views
from .views import *
from django.conf import settings

urlpatterns = [
	path('', views.index, name="index"),
]