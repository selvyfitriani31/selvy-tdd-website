# from django.test import TestCase
# from django.test.client import Client
# from django.urls import resolve
# from django.http import HttpRequest
# from .views import *
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# import time

# STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

# # Create your tests here.

# class story_6Test(TestCase):
# 	def test_hello_url_is_exist(self):
# 		response = Client().get('/hello/')
# 		self.assertEqual(response.status_code,200)

# 	def test_hello_using_halo_func(self):
# 		found = resolve('/hello/')
# 		self.assertEqual(found.func, halo)

# 	def test_halo_using_halo_template(self):
# 		response = Client().get('/hello/')
# 		self.assertTemplateUsed(response, 'halo.html')

# 	def test_landing_page_content_is_written(self):
# 		self.assertIsNotNone(landing_page_content)
# 		self.assertTrue(len(landing_page_content) >= 16)

# 	def test_landing_page_is_completed(self):
# 		request = HttpRequest()
# 		response = halo(request)
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Hello, Apa kabar?', html_response)
# 		self.assertIn(landing_page_content, html_response)

# 	def test_landing_page_is_completed(self):
# 		request = HttpRequest()
# 		response = halo(request)
# 		html_response = response.content.decode('utf8')
# 		self.assertIn('Hello, Apa kabar?', html_response)

# 	def test_form_validation_for_blank_items(self):
# 		form = Status_Form(data={'waktu': '', 'status': ''})
# 		self.assertFalse(form.is_valid())
# 		self.assertEqual(
# 			form.errors['status'],
# 			["This field is required."],
# 		)

# 	def test_model_can_create_new_status(self):
# 		new_status = Status.objects.create(waktu='2018-10-10 00:00:00', status='this is my status')
# 		counting_all_available_status = Status.objects.all().count()
# 		self.assertEqual(counting_all_available_status, 1)

# class story_6Functionaltest (TestCase) :
# 	def setUp(self):
# 		chrome_options = Options()
# 		chrome_options.add_argument('--dns-prefetch-disable')
# 		chrome_options.add_argument('--no-sandbox')
# 		chrome_options.add_argument('--headless')
# 		chrome_options.add_argument('disable-gpu')
# 		service_log_path = './chromedriver.log'
# 		service_args = ['--verbose']
# 		self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
# 		#self.selenium.implicitly_wait(25)
# 		self.selenium.maximize_window()
# 		super(story_6Functionaltest, self).setUp()
# 		# self.manage().window().maximize();
		
# 	def tearDown(self):
# 		self.selenium.quit()
# 		super(story_6Functionaltest, self).tearDown()

# 	def test_input_status(self):
# 		#Opening the link we want to test
# 		self.selenium.get('http://dandelion31.herokuapp.com/hello')
# 		time.sleep(5)

# 		#find the status_box
# 		status_box = self.selenium.find_element_by_id('id_status')
# 		submit = self.selenium.find_element_by_name('bagikan')

# 		#submit form
# 		status_box.send_keys('Coba Coba')
# 		time.sleep(5)
# 		submit.send_keys(Keys.RETURN)
# 		time.sleep(5)

# 		#find 'Coba Coba' in  my webpage
# 		self.assertIn("Coba Coba", self.selenium.page_source)

# 	#CHALLENGE 1 - test whether a layout's position correct - 2 example

# 	#--1 is waktu box in a correct position?
# 	def test_is_waktu_box_in_the_correct_position(self):
# 		self.selenium.get('http://dandelion31.herokuapp.com/hello')
# 		waktu_box = self.selenium.find_element_by_id('id_waktu')
# 		posisi = waktu_box.location
# 		posisi_y = posisi['y']
# 		self.assertEqual(posisi_y, 190)

# 	#--2 is status box in a correct position?
# 	def test_is_status_box_in_the_correct_position(self):
# 		self.selenium.get('http://dandelion31.herokuapp.com/hello')
# 		status_box = self.selenium.find_element_by_id('id_status')
# 		posisi = status_box.location
# 		posisi_y = posisi['y']
# 		self.assertEqual(posisi_y, 256)

# 	#CHALLENGE 2 - test whether my page using a correct css - 2 example

# 	#--1 is color of h1 pink?
# 	def test_is_color_of_h1_is_pink(self):
# 		self.selenium.get('http://dandelion31.herokuapp.com/hello')
# 		color = self.selenium.find_element_by_id('hello_kabar').value_of_css_property("color")
# 		self.assertEqual(color, 'rgba(255, 192, 203, 1)')
		
# 	#--2 is font size of h1 is 50px
# 	def test_is_font_size_of_h1_is_50px(self):
# 		self.selenium.get('http://dandelion31.herokuapp.com/hello')
# 		size = self.selenium.find_element_by_id('hello_kabar').value_of_css_property("font-size")
# 		self.assertEqual(size, '50px')




