from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Status(models.Model) :
	waktu = models.DateTimeField(default=timezone.now)
	status = models.CharField(max_length=300, null=True)