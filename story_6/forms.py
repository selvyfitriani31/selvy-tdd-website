from django import forms
from .models import Status
from django.forms import ModelForm

class Status_Form(forms.ModelForm):
	class Meta:
		model = Status
		fields = ['waktu', 'status',]
		widgets = {'waktu': forms.DateTimeInput(attrs={'class': 'form-control',}) ,
		'status': forms.TextInput(attrs={'class': 'form-control',}),
		}
