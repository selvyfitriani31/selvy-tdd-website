from django.shortcuts import render
from .forms import Status_Form		
from .models import Status
from django.http import HttpResponseRedirect
from django.urls import reverse 

# Create your views here.

landing_page_content = "Hello, Apa kabar?"
response = {}

def halo(request):
	my_status = Status.objects.all().values()
	if request.method == "POST":
		form = Status_Form(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/hello/')
	else :
		form = Status_Form()
	response = {'form' : form, 'my_status' : my_status}
	return render(request, 'halo.html', response)