from django.urls import path
from buku import views
from .views import *
from django.conf import settings

urlpatterns = [
	path('', views.index, name="index"),
	path('dataJSON/', views.dataJSON, name="dJson"),
]