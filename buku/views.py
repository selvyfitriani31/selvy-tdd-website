from django.shortcuts import render
import urllib.request, json, requests
from django.http import HttpResponseRedirect, HttpResponse

# Create your views here.
def index(request):
	return render(request, 'daftar_buku.html', {})

def dataJSON(request):
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
    return HttpResponse(data, content_type='application/json')
